#include "DayTimer.h"

//the constructor
DayTimer::DayTimer(int NumofDay)
    :DayNum(NumofDay)
{
    reset();
}

sf::Time DayTimer::getTime()
{
    return timeInDay; //used to compare the time that has passed in main to how much time should be in one day of the song
}


void DayTimer::reset()
{
    ifstream timefile("timing.txt");
    float placeholder;
    int NumofTimeLines = 0;

    if (timefile.is_open())
    {
        //To remind myself as much as anyone else. What is happening here is a loop that while the value of getline isnt greater than the amount of lines in text file, it will read each line
        //The value of temp line isnt equal to the line its on, but rather whatever  is present in templine. so if templine is at one, its the value in the first line.
        while (timefile >> placeholder) //while the file is still being read
        {
            if (NumofTimeLines == DayNum) //if the line we're on is equal to the day this instance was assigned
            {
                //i cant set time directly to an integer. However if i set time to 1 second, and then multiply it by an integer we get the same result
                timeInDay = sf::seconds(1);
                timeInDay = operator*(timeInDay, placeholder);
            }
            NumofTimeLines++; //read the next line
        }


        timefile.close();
    }
}
