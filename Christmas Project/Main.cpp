#include <SFML/Graphics.hpp>
#include <SFML\Audio.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include "Lyrics.h"
#include "DayTimer.h"
using namespace std;

int main()
{

    /*I want to explain how all this works at the start. So outside the file you'll find lyrics.txt and timer.txt, they house the lyrics and the time in each day respectively
    * what i do is call in a new days lyrics after the previous day has passed, so in sync with the song, calling in paragraphs at a time of the "x day of christmas"
    * plus all the gifts. I had to time out each day myself, and then put the time that day took into the timer file. However, if you were to do that for another song and
    * updated the number of values in the arrays accordingly, and obviously the lyrics file, this would be modular, and able to represent any song you want.
    * Originally i wanted to make it so that the lyrics would appear in tandem with their appearance in the song, but that was far too complicated so i changed to the day
    * system i use now, and am satisfied with just the paragraphs appearing at the right time */
    

    sf::Clock gameClock;
    //SETUP
    sf::RenderWindow window;
    window.create(sf::VideoMode::getDesktopMode(), "12 Days of Christmas", sf::Style::Titlebar | sf::Style::Close);
    sf::Font gameFont;
    gameFont.loadFromFile("Assets/Fonts/Christmas.ttf"); //A nice Christmas font i found

    //Title Text
    sf::Text TitleText;
    TitleText.setFont(gameFont);
    TitleText.setString("THE 12 DAYS OF CHRISTMAS");
    TitleText.setCharacterSize(32);
    TitleText.setFillColor(sf::Color::Red);
    TitleText.setPosition(window.getSize().x / 2 - TitleText.getLocalBounds().width / 2, 30);



    //Title Text
    std::vector<Lyrics> lyricsInstance;

    std::vector<DayTimer> DayInstances;
    int numDays = 12;  //Theres 12 days of Christmas after all
    for (int i = 0; i <= numDays; ++i) //<= because we also need to factor in the 10 seconds it takes to start. May not count as a Day in our time, but it does to an Array
    {
        DayInstances.push_back(DayTimer(i)); //Send a new day, using the I as the day it is and what days time it should track
    }



    //A nice christmassy background
    sf::Texture backTexture;
    backTexture.loadFromFile("Assets/Background/Background.jpg");
    sf::Sprite background;
    background.setTexture(backTexture);

    //Plays the song
    sf::Music ChristmasMusic;
    ChristmasMusic.openFromFile("Assets/Music/music.ogg");
    ChristmasMusic.play();

    int Day = 0; //Tracks what day of Christmas we are on
    int lineCoutner = 0; //Tracks what line of the file we are on

    sf::Time Timekeeper; //Controls the time each day gets

    //Running
    while (window.isOpen())
    {

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        sf::Time frameTime = gameClock.restart();
        Timekeeper += frameTime;

        //Once the time has passed on this verse, and another is about to start we run this
        if (DayInstances[Day].getTime() <= Timekeeper) 
        {
            Day++; //Set it to be the next day of Christmas
            if (Day == 13) //To make sure i dont crash at the end of the program
            {
                Day = 12;
            }
            Timekeeper = sf::seconds(0); //Reset the timekeeper back to 0 seconds for the new Day
            lyricsInstance.clear(); //Reset the lyrics so we can put in the new days ones
            for (int i = 0; i <= (Day + 2); i++) //Run this the the amount of gifts being granted, +2 for the "on the X day my true love gave to me"
            {
                lyricsInstance.push_back(Lyrics(gameFont, window.getSize(), lineCoutner, i));
                lineCoutner++;
            }
        }


        window.clear();
        window.draw(background);
        window.draw(TitleText);
        for (int i = 0; i < lyricsInstance.size(); i++)
        {
            lyricsInstance[i].draw(window);
        }

        window.display();
    }

    return 0;
}