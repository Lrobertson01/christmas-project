#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <string>
#pragma once
using namespace std;

class DayTimer
{
private:
	sf::Time timeInDay; //Reads how long each day is
	int DayNum; //Sets what day it is

public:
	DayTimer(int NumofDay); // the constructor, passes in what day it is
	sf::Time getTime(); //a function to determine how much time is in that specific day, used to compare against how much time has passed in main
	void reset(); //the reset function that assigns the time values
};

