#include "Lyrics.h"

Lyrics::Lyrics(sf::Font& mainFont, sf::Vector2u screensize, int numline, int positionline)
    :linenum(numline)
    , positionnum(positionline)
{
    reset(mainFont, screensize);
}

void Lyrics::reset(sf::Font& mainFont, sf::Vector2u screensize)
{
    MainText.setFont(mainFont);
    MainText.setCharacterSize(32);
    MainText.setFillColor(sf::Color::Red);
    MainText.setPosition(screensize.x / 2 - MainText.getLocalBounds().width / 2, screensize.y / 2);
    MainText.setString("");

    ifstream textFile;
    string templine;
    textFile.open("MainText.txt"); //A text file containing paragraphs of each  day of the song
    int NumOfLines = 0;

    if (textFile.is_open())
    {
        //To remind myself as much as anyone else. What is happening here is a loop that while the value of getline isnt greater than the amount of lines in text file, it will read each line
        //The value of temp line isnt equal to the line its on, but rather whatever  is present in templine. so if templine is at one, its the value in the first line.
        while (getline(textFile, templine)) //while still reading the file/has not reached the end yet
        {
            if (NumOfLines == linenum) //If the line being read is equal to the line assigned to this instance of the class
            {
                MainText.setString(templine); //Set the text to be equal to whats on the line being read
                //Set the position to be starting at a quarter down the screen, and then for each line moving slightly down
                MainText.setPosition(screensize.x / 2 - MainText.getLocalBounds().width / 2, screensize.y /4 + (40 * positionnum));
            }
            NumOfLines++; //read the next line
        }

        textFile.close();
    }


}





void Lyrics::draw(sf::RenderWindow& gameWindow)
{
    gameWindow.draw(MainText);
}