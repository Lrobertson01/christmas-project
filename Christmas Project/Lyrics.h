#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <string>
#pragma once
using namespace std;


class Lyrics
{
public: 
    Lyrics(sf::Font& mainFont, sf::Vector2u screensize, int numline, int positionline); //The constructor
    void reset(sf::Font& mainFont,sf::Vector2u screensize); //The reset function that creates them and reads the lyrics
    void draw(sf::RenderWindow& gameWindow); //The draw function


private: 
    sf::Text MainText; //The text itself
    int linenum; //The number of the line it should be reading
    int positionnum; //The number that determines where on screen it is
};

